package com.sergialmar.wschat.util;

import com.sergialmar.wschat.domain.User;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by El on 19.07.2016.
 */
public class UserManager {
    static UserManager userManager;
    Map<String, User> users=new ConcurrentHashMap<>();
    public static UserManager getUserManager() {
        if(userManager==null){
            userManager=new UserManager();
        }
//        userManager.users.put("Vasya", new User("Vasya"));
//        userManager.users.put("Petya", new User("Petya"));
        return userManager;
    }
    public User getUserByName(String name){
        if(!users.containsKey(name)){
            users.put(name, new User(name));
        }
        return users.get(name);
    }
    public User addUser(User user){
        return users.put(user.getName(), user);
    }

}
