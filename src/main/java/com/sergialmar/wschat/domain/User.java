package com.sergialmar.wschat.domain;

/**
 * Created by El on 19.07.2016.
 */
public class User implements Comparable<User> {
    String name;

    public User(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int compareTo(User o) {
        return -1;
    }
}
