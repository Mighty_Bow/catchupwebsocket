package com.sergialmar.wschat.web;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sergialmar.wschat.Game.GameKeeper;
import com.sergialmar.wschat.Game.GamesRepository;
import com.sergialmar.wschat.Game.messages.InfoGameStart;
import com.sergialmar.wschat.domain.ChatMessage;
import com.sergialmar.wschat.domain.User;
import com.sergialmar.wschat.event.ParticipantRepository;
import com.sergialmar.wschat.util.UserManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.Message;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Controller;

import java.security.Principal;
import java.util.Iterator;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Created by El on 19.07.2016.
 */
@Controller
public class SearchController {
    @Autowired private ParticipantRepository participantRepository;
    @Autowired private SimpMessagingTemplate simpMessagingTemplate;
    @Autowired private GamesRepository games;
    @Autowired ObjectMapper mapper;
    CopyOnWriteArrayList<User> queue = new CopyOnWriteArrayList<>();

    @MessageMapping("/search.start")
    public void startSearch(@Payload ChatMessage message, Principal principal) {
        UserManager.getUserManager().addUser(new User(principal.getName()));
        queue.add(UserManager.getUserManager().getUserByName(principal.getName()));
        queue.sort((User u1, User u2) ->{return -1;});
        message.setMessage("You add into queue");
        message.setUsername("SERVER");
        simpMessagingTemplate.convertAndSend("/user/" + principal.getName() + "/exchange/amq.direct/search.status", message);
    }

    @MessageMapping("/game.move.{username}")
    void withFriend(Principal principal){

    }

    @Scheduled(fixedDelay = 1000)
    void searching() {
        if (queue.size() < 1) return;

        User last = null;
        for (int i = 0; i < queue.size(); i++) {
            User current = queue.get(i);
            if (last != null) {
                startGame(current, last);
                queue.remove(current);
                queue.remove(last);
                i -= 2;
            }
            last = i>=0? queue.get(i):null;
        }
    }

    void startGame(User u1, User u2){
        GameKeeper game= new GameKeeper(u1, u2);
        games.addGame(u1.getName(), game);
        games.addGame(u2.getName(), game);
        ChatMessage message = new ChatMessage();
        message.setUsername("SERVER");
        message.setMessage("Game find, your opponent " + u2.getName());
        try {
            InfoGameStart info = new InfoGameStart(1, true, u2.getName());
            System.out.println(mapper.writeValueAsString(info));
            Message<byte[]> mes = MessageBuilder.withPayload(mapper.writeValueAsBytes(info)).build();
            System.out.println(mes.toString());

            simpMessagingTemplate.send("/user/" + u1.getName() + "/exchange/amq.direct/search.find", MessageBuilder.withPayload(mapper.writeValueAsBytes(info)).build());
            message.setMessage("Game find, your opponent " + u1.getName());
            info = new InfoGameStart(2, true, u1.getName());
            simpMessagingTemplate.send("/user/" + u2.getName() + "/exchange/amq.direct/search.find", MessageBuilder.withPayload(mapper.writeValueAsBytes(info)).build());
        }catch (Exception e){e.printStackTrace();}

    }
}
