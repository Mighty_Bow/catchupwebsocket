package com.sergialmar.wschat.web;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sergialmar.wschat.Game.GameAction;
import com.sergialmar.wschat.Game.GameKeeper;
import com.sergialmar.wschat.Game.GamesRepository;
import com.sergialmar.wschat.Game.messages.ActionContext;
import com.sergialmar.wschat.Game.messages.MoveMessage;
import com.sergialmar.wschat.domain.ChatMessage;
import com.sergialmar.wschat.event.ParticipantRepository;
import com.sergialmar.wschat.util.UserManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Controller;

import java.security.Principal;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by El on 29.07.2016.
 */
@Controller
public class GameController {
    @Autowired
    private ParticipantRepository participantRepository;
    @Autowired
    private SimpMessagingTemplate simpMessagingTemplate;
    @Autowired
    private GamesRepository games;
    @Autowired
    ObjectMapper mapper;

//    @SubscribeMapping("/user/exchange/amq.direct/game.moveop")
//    public ActionContext retrieveParticipants(Principal principal) {
//        if()
//        return games.get(principal.getName()).nextAction(UserManager.getUserManager().getUserByName(principal.getName()));
//    }

//    @MessageMapping("/game.move")
//    void move(@Payload MoveMessage message, Principal principal){
//
//    }

    @MessageMapping("/game.move.{username}")
    public void makeAction(@Payload MoveMessage message, @DestinationVariable("username") String username, Principal principal) {
        int result = games.get(username).makeAction(new GameAction(message));
        if (result == -1)
            return;
        if (result > 0) {
            sendEndGame(games.get(username));
        }
        ActionContext ac = games.get(username).nextAction(UserManager.getUserManager().getUserByName(username));
        ac.setMoves(message.getMoves());
        ac.setPlayer(message.getPlayer());
        try {
            simpMessagingTemplate.send("/user/" + username + "/exchange/amq.direct/game.opmove", MessageBuilder.withPayload(mapper.writeValueAsBytes(ac)).build());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void sendEndGame(GameKeeper game) {
        ChatMessage mes1 = new ChatMessage();
        ChatMessage mes2 = new ChatMessage();
        if (game.getStatus() == 3) {
            mes1.setMessage("NICHYA");
            mes2.setMessage("NICHYA");
        } else if (game.getStatus() == 1) {
            mes1.setMessage("YOU WIN!!!111");
            mes2.setMessage("YOU LOSE!!!!111");
        } else if (game.getStatus() == 2) {
            mes2.setMessage("YOU WIN!!!111");
            mes1.setMessage("YOU LOSE!!!!111");
        }
        try {
            simpMessagingTemplate.send("/user/" + game.getUser1() + "/exchange/amq.direct/game.end", MessageBuilder.withPayload(mapper.writeValueAsBytes(mes1)).build());
            simpMessagingTemplate.send("/user/" + game.getUser1() + "/exchange/amq.direct/game.end", MessageBuilder.withPayload(mapper.writeValueAsBytes(mes2)).build());
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
