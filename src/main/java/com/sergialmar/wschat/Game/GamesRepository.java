package com.sergialmar.wschat.Game;

import org.springframework.context.annotation.Bean;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by El on 01.08.2016.
 */

public class GamesRepository {
    private Map<String, GameKeeper> games=new ConcurrentHashMap<>();
    public void addGame(String name, GameKeeper game){
        games.put(name,game);
    }

    public Map<String, GameKeeper> getGames() {
        return games;
    }
    public GameKeeper get(String name){
        return games.get(name);
    }
}
