package com.sergialmar.wschat.Game;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by El on 29.07.2016.
 */
public class HexBoard {
    Hex[][] mapHex;
    int counter=0;
    ArrayList<ArrayList<Hex>> grooups=new ArrayList<>();
    int N;
    ArrayList<Hex> all=new ArrayList<>();
    ArrayList<Hex> free=new ArrayList<Hex>();
    public HexBoard(int n) {
        N = n;
        mapHex=new Hex[N][N];
        N=(n-1)/2;
        int offset=0,offsetq=N;
        for (int i=0,r=N+1,cur=0;i<N+N+1;i++) {
            for (int j = 0; j < r; j++) {
                mapHex[i][j+offsetq]=new Hex(i-N,j-offset);
                all.add(mapHex[i][j+offsetq]);
                free.add(mapHex[i][j+offsetq]);
            }
            if (i < N) {
                r++;
                offsetq--;
                offset++;
            }
            else {
                r--;
            }
        }
    }
    public Hex getHex(int r,int q){
        if(r<-N || r>N || q<-N || q>N) return null;
        return mapHex[r+N][q+N];
    }
    public ArrayList<Hex> getNeighbours(Hex hex){

        return getNeighbours(hex.r, hex.q);
    }
    public ArrayList<Hex> getNeighbours(int r, int q){
        ArrayList<Hex> nei=new ArrayList<>();
        if(getHex(r,q-1)!=null) nei.add(getHex(r,q-1));
        if(getHex(r,q+1)!=null) nei.add(getHex(r,q+1));
        if(getHex(r-1,q)!=null) nei.add(getHex(r-1,q));
        if(getHex(r-1,q+1)!=null) nei.add(getHex(r-1,q+1));
        if(getHex(r+1,q)!=null) nei.add(getHex(r+1,q));
        if(getHex(r+1,q-1)!=null) nei.add(getHex(r+1,q-1));
        return nei;
    }
    void setHex(Hex hex){
        Hex h=getHex(hex.r, hex.q);
        if(h.getPlayer()==0){
            h.setPlayer(hex.getPlayer());
        }
        addToGroups(h);
    }
    public int score(int player){
        return grooups.stream().filter((g)->g!=null).mapToInt((g)->g.size()).max().orElse(0);
    }
    public void addToGroups(Hex hex) {
        boolean one = true;
        int many = 0;
        for (Hex h : getNeighbours(hex)) {
            if (h.player == hex.player && h.groupId != -1) {
                one = false;
                if (hex.groupId == -1) {
                    hex.groupId = h.groupId;
                    grooups.get(h.groupId).add(hex);
                }
                if (h.groupId != hex.groupId) {
                    int lastId = hex.groupId;
                    for (Hex gh : grooups.get(lastId)) {
                        gh.groupId = h.groupId;
                        grooups.get(h.groupId).add(gh);
                    }
                    many++;
                    grooups.set(lastId, null);
                }
            }
        }
        if (one) {
            ArrayList<Hex> newAr = new ArrayList<Hex>();
            newAr.add(hex);
            grooups.add(newAr);
            hex.groupId = counter;
            counter++;
        }
    }
    public int getWinner(){
        List<Integer> scores1=scoreAllDistrict(1);
        List<Integer> scores2=scoreAllDistrict(2);
        for(int i=0;i<scores1.size() && i<scores2.size();i++){
            if(scores1.get(i)>scores2.get(i)) return 1;
            else if(scores2.get(i)>scores1.get(i)) return 2;
        }
        if(scores1.size()>scores2.size()) return 1;
        if(scores2.size()>scores1.size()) return 2;
        return 3;
    }
    List<Integer> scoreAllDistrict(int player){
        return grooups.stream().filter((d)->d!=null).filter((d)->d.get(0).getPlayer()==player).
                mapToInt((d)->d.size()).boxed().collect(Collectors.toList());
    }
    public static class Hex{
        int r, q;
        int player=0;
        int groupId=-1;
        public Hex(int r, int q) {
            this.r = r;
            this.q = q;
        }

        public Hex(int r, int q, int player) {
            this.r = r;
            this.q = q;
            this.player = player;
        }

        public int getPlayer() {
            return player;
        }

        public void setPlayer(int player) {
            this.player = player;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Hex hex = (Hex) o;

            if (r != hex.r) return false;
            return q == hex.q;

        }

        @Override
        public int hashCode() {
            int result = r;
            result = 31 * result + q;
            return result;
        }
    }
}
