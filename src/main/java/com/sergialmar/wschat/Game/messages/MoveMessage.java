package com.sergialmar.wschat.Game.messages;

/**
 * Created by El on 01.08.2016.
 */
public class MoveMessage {
    int[] moves;
    int player;

    public MoveMessage() {
    }

    public MoveMessage(int[] moves, int player) {
        this.moves = moves;
        this.player = player;
    }

    public int[] getMoves() {
        return moves;
    }

    public void setMoves(int[] moves) {
        this.moves = moves;
    }

    public int getPlayer() {
        return player;
    }

    public void setPlayer(int player) {
        this.player = player;
    }
}
