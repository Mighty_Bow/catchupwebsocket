package com.sergialmar.wschat.Game.messages;

/**
 * Created by El on 30.07.2016.
 */
public class ActionContext {
    int possibleMoves;
    int scoreOp;
    int[] moves;
    int player;
    public ActionContext() {
    }

    public ActionContext(int possibleMoves, int scoreOp) {
        this.possibleMoves = possibleMoves;
        this.scoreOp = scoreOp;
    }

    public int getPossibleMoves() {
        return possibleMoves;
    }

    public void setPossibleMoves(int possibleMoves) {
        this.possibleMoves = possibleMoves;
    }

    public int getScoreOp() {
        return scoreOp;
    }

    public void setScoreOp(int scoreOp) {
        this.scoreOp = scoreOp;
    }

    public int[] getMoves() {
        return moves;
    }

    public void setMoves(int[] moves) {
        this.moves = moves;
    }

    public int getPlayer() {
        return player;
    }

    public void setPlayer(int player) {
        this.player = player;
    }
}
