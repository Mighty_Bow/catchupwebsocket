package com.sergialmar.wschat.Game.messages;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by El on 31.07.2016.
 */
@XmlRootElement
public class InfoGameStart {
    private int player;
    private boolean status;
    private String opName;

    public InfoGameStart() {
    }

    public InfoGameStart(int player, boolean status, String opName) {
        this.player = player;
        this.status = status;
        this.opName = opName;
    }
    @Override
    public String toString() {
        return "InfoGameStart [opName=" + opName + ", status=" + status +
                ", player=" + player+"]";
    }

    public int getPlayer() {
        return player;
    }

    public void setPlayer(int player) {
        this.player = player;
    }

    public boolean isStatus() {
        return status;
    }
    public boolean getStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getOpName() {
        return opName;
    }

    public void setOpName(String opName) {
        this.opName = opName;
    }
}
