package com.sergialmar.wschat.Game;

import com.sergialmar.wschat.Game.messages.MoveMessage;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by El on 29.07.2016.
 */
public class GameAction {
    List<HexBoard.Hex> moves;

    public GameAction(List<HexBoard.Hex> moves) {
        this.moves = moves;
    }
    public GameAction(List<Integer> moves, int player) {
        this.moves=new ArrayList<>();
        for (int i=0;i<moves.size();i+=2){
            this.moves.add(new HexBoard.Hex(moves.get(i), moves.get(i+1), player));
        }
    }
    public GameAction(MoveMessage move){
        this.moves=new ArrayList<>();
        for (int i=0;i<move.getMoves().length;i+=2){
            this.moves.add(new HexBoard.Hex(move.getMoves()[i], move.getMoves()[i+1], move.getPlayer()));
        }
    }
    public List<HexBoard.Hex> getMoves() {
        return moves;
    }
}
