package com.sergialmar.wschat.Game;

import com.sergialmar.wschat.Game.messages.ActionContext;
import com.sergialmar.wschat.domain.User;

import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Created by El on 29.07.2016.
 */
public class GameKeeper {
    final int SIZE=9;
    HexBoard board;
    int []score;
    User user1, user2;
    int posibleMoves=1;
    int curPlayer=1;
    User winner;
    int status;
    AtomicBoolean start = new AtomicBoolean(false);
    public GameKeeper(User user1, User user2) {
        board=new HexBoard(SIZE);
        score=new int[2];
        this.user1=user1;
        this.user2=user2;
    }
    public int makeAction(GameAction act){
        if(act.getMoves().get(0).getPlayer()!=curPlayer) return -1;
        curPlayer=3-curPlayer;
        boolean fgs=score[0]>score[1];
        boolean sgf=score[1]>score[0];
        act.getMoves().stream().peek(board::setHex);
        score[0]=board.score(1);
        score[1]=board.score(2);
        posibleMoves=2;
        if((fgs && score[1]>score[0]) || (sgf && score[0]>score[1])) posibleMoves=3;
        if(isOver()){
            int status=board.getWinner();
            if(status==1)winner=user1;
            if(status==2)winner=user2;
            return status;
        }
        return 0;
    }
    public User getOpponent(String name){
        if(name.equals(user1.getName())) return user2;
        return user1;
    }
    public ActionContext nextAction(User user){
        if(user==user1){
            return new ActionContext(posibleMoves,score[1]);
        }
        else if(user==user2){
            return new ActionContext(posibleMoves,score[0]);
        }
        return null;
    }
    public AtomicBoolean getStart() {
        return start;
    }
    public int countPosibleMoves(){
        return posibleMoves;
    }
    public boolean isOver(){
        if(board.free.size()==0) return true;
        int min,max;
        if(score[0]>score[1]){
            max=score[0];
            min=score[1];
        }
        else{
            max=score[1];
            min=score[0];
        }
        if(board.free.size()+min<max) return true;
        return false;
    }
    public User getUserById(int id){
        if(id==1) return user1;
        return user2;
    }

    public User getUser1() {
        return user1;
    }

    public User getUser2() {
        return user2;
    }

    public int getStatus() {
        return status;
    }

    public User getWinner() {
        return winner;
    }
}
